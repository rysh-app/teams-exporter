#!/usr/bin/python

import sys
import requests
import json
import yaml
import pytz
import os
import os.path
from datetime import datetime
from jinja2 import Template, Environment, FileSystemLoader

# read config file
try:
	f = open("config.yaml","r")
except:
	print("Cannot open config.yaml")
	sys.exit()
else:
	conf = yaml.load(f)
	f.close()
TOKEN = conf['token']
TIMEZONE = conf['timezone']
EXPORT_FILES = conf['export_files']

global_file_counter = 1 # used as prefix of attached file
###

def get_localtime(iso_str):
	# modify timestamp format
    dt = None
    try:
        dt = datetime.strptime(iso_str, '%Y-%m-%dT%H:%M:%S.%fZ')
        dt = pytz.utc.localize(dt).astimezone(pytz.timezone(TIMEZONE))
    except ValueError:
        try:
            dt = datetime.strptime(iso_str, '%Y-%m-%dT%H:%M:%S.%f%z')
            dt = dt.astimezone(pytz.timezone(TIMEZONE))
        except ValueError:
            pass
    return dt.strftime('%Y/%m/%d %H:%M:%S')

def get_room_list():
	# get Room List
	spark_url = 'https://api.ciscospark.com/v1/rooms'

	headers = {
		'Authorization' : 'Bearer %s' % TOKEN,
		'Content-Type' : 'application/json'
	}
	params = {
		'max': 9999
	}
	rs = requests.get(spark_url, headers = headers, params = params)
	if rs.status_code == 401:
		print("Unauthorized access. Token may be invalid")
		sys.exit()
	elif rs.status_code != 200:
		print("Unexpected response: code %d" % rs.status_code) 
		sys.exit()

	return(rs.json())
def get_message_list(room_id):
	# get message list
	spark_url = 'https://api.ciscospark.com/v1/messages'

	headers = {
		'Authorization' : 'Bearer %s' % TOKEN,
		'Content-Type' : 'application/json'
	}
	params = {
		'roomId': room_id,
		'max': 100000
	}
	rs = requests.get(spark_url, headers = headers, params = params)
	if rs.status_code == 401:
		print("Unauthorized access. Token may be invalid")
		sys.exit()
	elif rs.status_code != 200:
		print("Unexpected response: code %d" % rs.status_code) 
		sys.exit()

	return(rs.json())
def sanitize(text):
	text = text.replace('>','&gt;')
	text = text.replace('<','&lt;')
	text = text.replace("\n",'<br>')
	return(text)
def get_filename(title):
		title = title.replace('/','-')
		return('msg/' + title + '.html')
def get_attachment(url):
	headers = {
		'Authorization' : 'Bearer %s' % TOKEN,
		'Content-Type' : 'application/json'
	}
	rs = None
	if EXPORT_FILES:
		rs = requests.get(url, headers = headers)
	else:
		rs = requests.head(url, headers = headers)

	if rs.status_code == 401:
		print("Unauthorized access. Token may be invalid")
		sys.exit()
	elif rs.status_code != 200:
		print("Unexpected response: code %d for %s" % (rs.status_code,url)) 

	# extract filename from HTTP header
	filename_orig = rs.headers['Content-Disposition']
	filename_orig = filename_orig.replace('attachment; filename="','')
	filename_orig = filename_orig.replace('"','')
	filename_orig = filename_orig.replace(':','-')
	filename_saved = str(global_file_counter) + "_" + filename_orig
	fn, ext = os.path.splitext(filename_saved) # get file extension
	is_image = False
	if(ext.lower() == '.jpg' or ext.lower() == '.png' or ext.lower() == '.gif'):
		is_image = True

	if EXPORT_FILES:
		try:
			with open("./msg/files/" + filename_saved, 'wb') as af:
				af.write(rs.content)
		except:
			print("Cannot save attached file: %s" % filename_saved)
		else:
			print("Attached file saved: " + filename_saved)

	return(filename_orig,filename_saved,is_image)

##### Main #####
env = Environment(loader=FileSystemLoader('./', encoding='utf8'))
tmpl = env.get_template('msg.tmpl')

try:
	os.makedirs('msg', exist_ok=True) # create folder for messages
	if EXPORT_FILES:
		os.makedirs('msg/files', exist_ok=True) # create folder for attachments
except:
	print("Cannot create folder")

room_list = get_room_list()
num = len(room_list['items'])
print('# of Rooms:%d' % num)
for i,room in enumerate(room_list['items']):
	print('exporting %d / %d' % (i+1,num))

	filename = get_filename(room['title']) # HTML file name
	try:
		f = open(filename,'w')
	except:
		print("Cannot create file: %s" % filename)
		sys.exit()

	msg_list = []
	m_list = get_message_list(room['id'])
	for msg in m_list['items']:
		timestamp = get_localtime(msg['created'])
		sender = ''
		text = ''
		f_list = []
		if('personEmail' in msg):
			sender = msg['personEmail']
		else:
			sender = ''
		if('text' in msg):
			text = sanitize(msg['text'])
		if ('files' in msg):
			for item in msg['files']:
				filename_orig, filename_saved, is_image = get_attachment(item)
				f_list.append({'url':item,'filename_orig':filename_orig,'filename_saved':filename_saved,'is_image':is_image})
				global_file_counter+=1
		msg_list.append({
			'email':sender,
			'datetime':timestamp,
			'text': text,
			'f_list':f_list
		})	
			
	msg_list.reverse() # old to new
	html = tmpl.render({"msg_list":msg_list, "export_files":EXPORT_FILES})
	f.write(html)
	f.close()

