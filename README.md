## How to use

0. Install required modules
    pip3 install -r requirements.txt  
1. Get your access token from https://developer.webex.com/  
    You can find your token in "GUIDES" > "Getting Started" > "Authentication" section.  
    Once you logout from the site, the token will be expired and you need to re-login and get a new token.  
2. Edit config.yaml.sample and rename to config.yaml  
		token: API Access Token  
		timezone: Your timezone e.g. Asia/Tokyo  
		export_files: yes or no if 'yes', attached files are exported.  
3. Run "python3 export.py"
4. HTML files are created in "msg" folder

## Known issue
Attached file name with Japanese character is not displayed correctly.
